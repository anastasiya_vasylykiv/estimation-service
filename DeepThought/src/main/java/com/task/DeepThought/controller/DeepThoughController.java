package com.task.DeepThought.controller;

import com.task.DeepThought.api.DeepThoughAPI;
import com.task.DeepThought.service.DeepThoughService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DeepThoughController implements DeepThoughAPI {

    private DeepThoughService deepThoughService;

    public DeepThoughController(DeepThoughService deepThoughService) {
        this.deepThoughService = deepThoughService;
    }

    @Override
    @GetMapping("/calculateStoryPoints")
    public ResponseEntity<Double> calculateStoryPoints(@RequestParam Integer hours) {
        return ResponseEntity.ok(this.deepThoughService.calculateStoryPoints(hours.doubleValue()));
    }
}
