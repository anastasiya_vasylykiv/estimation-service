package com.task.DeepThought.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface DeepThoughAPI {

    @GetMapping("/calculateStoryPoints")
    ResponseEntity<Double> calculateStoryPoints(@RequestParam Integer hours);
}
