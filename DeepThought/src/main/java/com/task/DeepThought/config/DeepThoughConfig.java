package com.task.DeepThought.config;

import com.task.DeepThought.controller.DeepThoughController;
import com.task.DeepThought.service.DeepThoughService;

public class DeepThoughConfig {
    DeepThoughController deepThoughController(final DeepThoughService deepThoughService) {
        return new DeepThoughController(deepThoughService);
    }
}
