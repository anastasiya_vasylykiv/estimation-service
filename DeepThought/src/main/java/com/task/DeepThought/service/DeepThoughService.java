package com.task.DeepThought.service;

import org.springframework.stereotype.Service;

@Service
public class DeepThoughService {
    public Double calculateStoryPoints(Double hours){
        return hours/4;
    }
}
