package com.example.developer.model;

import com.example.developer.validator.ValidUserStory;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Estimate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ValidUserStory
    @Column(name = "user_story")
    private String userStory;
    @Column(name =  "story_points")
    private double storyPoints;
    private int hours;
    private LocalDate date;

    public String getUserStory() {
        return userStory;
    }

    public void setUserStory(String userStory) {
        this.userStory = userStory;
    }

    public double getStoryPoints() {
        return storyPoints;
    }

    public void setStoryPoints(double storyPoints) {
        this.storyPoints = storyPoints;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public LocalDate getDate() { return date; }

    public void setDate(LocalDate date) { this.date = date; }
}
