package com.example.developer.model;

public class History {

    private double numberOfStoryPointsInTwoLastWeeks;
    private long totalNumberOfEstimatedUserStories;

    public double getNumberOfStoryPointsInTwoLastWeeks() {
        return numberOfStoryPointsInTwoLastWeeks;
    }

    public void setNumberOfStoryPointsInTwoLastWeeks(double numberOfStoryPointsInTwoLastWeeks) {
        this.numberOfStoryPointsInTwoLastWeeks = numberOfStoryPointsInTwoLastWeeks;
    }

    public long getTotalNumberOfEstimatedUserStories() {
        return totalNumberOfEstimatedUserStories;
    }

    public void setTotalNumberOfEstimatedUserStories(long totalNumberOfEstimatedUserStories) {
        this.totalNumberOfEstimatedUserStories = totalNumberOfEstimatedUserStories;
    }
}
