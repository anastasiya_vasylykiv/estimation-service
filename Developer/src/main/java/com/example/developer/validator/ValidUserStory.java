package com.example.developer.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = StoryValidator.class)
@Documented
public @interface ValidUserStory {
    String message() default "Invalid name of the user story";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}