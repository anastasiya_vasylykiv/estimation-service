package com.example.developer.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StoryValidator
        implements ConstraintValidator<ValidUserStory, String> {

    private static final String STORY_PATTERN = "^ADI-[0-9]{3,5}$";

    @Override
    public void initialize(ValidUserStory constraintAnnotation) {
    }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext context) {
        return (validateUserStory(email));
    }

    private boolean validateUserStory(String email) {
        Pattern pattern = Pattern.compile(STORY_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}