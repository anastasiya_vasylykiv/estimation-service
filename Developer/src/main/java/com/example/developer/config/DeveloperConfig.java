package com.example.developer.config;

import com.example.developer.controller.DeveloperController;
import com.example.developer.repository.EstimateRepository;
import com.example.developer.repository.HibernateOperations;
import com.example.developer.service.EstimationService;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DeveloperConfig {

    DeveloperController developerController(EstimationService estimationService, EstimateRepository estimateRepository,
                                            HibernateOperations hibernateOperations) {
        return new DeveloperController(estimationService, estimateRepository, hibernateOperations);
    }
}
