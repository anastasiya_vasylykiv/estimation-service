package com.example.developer.controller;

import com.example.developer.api.DeveloperAPI;
import com.example.developer.model.Estimate;
import com.example.developer.model.History;
import com.example.developer.repository.EstimateRepository;
import com.example.developer.repository.HibernateOperations;
import com.example.developer.service.EstimationService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
public class DeveloperController implements DeveloperAPI {

    private EstimationService estimationService;

    private EstimateRepository estimateRepository;

    private HibernateOperations hibernateOperations;

    public DeveloperController(EstimationService estimationService, EstimateRepository estimateRepository, HibernateOperations hibernateOperations) {
        this.estimationService = estimationService;
        this.estimateRepository = estimateRepository;
        this.hibernateOperations = hibernateOperations;
    }

    @PreAuthorize("hasRole('TL') or hasRole('PM')")
    @GetMapping("/estimate")
    public String estimate(Model model) {
        return this.estimationService.estimate(model);
    }

    @PreAuthorize("hasRole('TL') or hasRole('PM')")
    @PostMapping("/estimate")
    public String estimateSubmit(@Valid @ModelAttribute Estimate estimate, Authentication authentication) {
        final String estimation = this.estimationService.estimateSubmit(estimate, authentication);
        //this.estimateRepository.save(estimate);
        this.hibernateOperations.saveEstimate(estimate);
        return estimation;
    }

    @PreAuthorize("hasRole('TL') or hasRole('PM')")
    @GetMapping("/history")
    public History showHistory(@Valid @ModelAttribute History history) {
//        history.setTotalNumberOfEstimatedUserStories(this.estimateRepository.countAllUserStories());
//        history.setNumberOfStoryPointsInTwoLastWeeks(this.estimateRepository.countStoryPointsByDateAfter(LocalDate.now().minusWeeks(2)));
       history.setTotalNumberOfEstimatedUserStories(this.hibernateOperations.countAllUserStories());
       history.setNumberOfStoryPointsInTwoLastWeeks(this.hibernateOperations.countStoryPointsByDateAfter(LocalDate.now().minusWeeks(2)));
        return history;
    }
}
