package com.example.developer.api;

import com.example.developer.model.Estimate;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

public interface DeveloperAPI {
    @GetMapping("/estimate")
    String estimate(Model model);

    @PostMapping("/estimate")
    String estimateSubmit(@Valid @ModelAttribute Estimate estimate, Authentication authentication);
}
