package com.example.developer.repository;

import com.example.developer.model.Estimate;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Repository
public interface EstimateRepository extends CrudRepository<Estimate, Long> {

    @Query(value = "SELECT count(DISTINCT user_story) FROM estimate ", nativeQuery = true)
    long countAllUserStories();

    @Query(value = "SELECT sum(story_points) FROM estimate WHERE date>=?1", nativeQuery = true)
    double countStoryPointsByDateAfter(LocalDate localDate);
}