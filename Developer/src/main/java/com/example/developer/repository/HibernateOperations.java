package com.example.developer.repository;

import com.example.developer.model.Estimate;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;

/**
 * Class to illustrate the usage of EntityManager API.
 */
@Service
public class HibernateOperations {

    private static final EntityManagerFactory emf;

    /**
     * Static block for creating EntityManagerFactory. The Persistence class looks for META-INF/persistence.xml in the classpath.
     */
    static {
        emf = Persistence.createEntityManagerFactory("Estimate");
    }

    /**
     * Static method returning EntityManager.
     *
     * @return EntityManager
     */
    private static EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void saveEstimate(Estimate estimate) {
        EntityManager em = HibernateOperations.getEntityManager();
        em.getTransaction()
                .begin();
        em.persist(estimate);
        em.getTransaction()
                .commit();
    }

    public Long countAllUserStories() {
        EntityManager em = HibernateOperations.getEntityManager();
        return (Long) em.createQuery("SELECT count(DISTINCT user_story) FROM estimate")
                .getSingleResult();
    }

    public Double countStoryPointsByDateAfter(LocalDate date) {
        EntityManager em = HibernateOperations.getEntityManager();
        return (Double) em.createQuery("SELECT sum(story_points) FROM estimate WHERE date>=?1")
                .setParameter(1, date)
                .getSingleResult();
    }
}