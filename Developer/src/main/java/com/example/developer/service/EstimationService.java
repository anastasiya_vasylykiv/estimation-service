package com.example.developer.service;

import com.example.developer.model.Estimate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Random;

@Service
public class EstimationService {
    public String estimate(Model model) {
        model.addAttribute("userStory", new Estimate());
        return "estimate";
    }

    public String estimateSubmit(Estimate estimate, Authentication authentication) {
        boolean isPM = authentication.getAuthorities().stream().anyMatch(x -> "ROLE_PM".equals(x.getAuthority()));
        Random random = new Random();
        int hours = random.nextInt(50);
        double storyPoints = calculateStoryPoints(hours);
        estimate.setHours(hours);
        estimate.setStoryPoints(isPM ? storyPoints / 2 : storyPoints);
        estimate.setDate(LocalDate.now());
        return "result";
    }

    private double calculateStoryPoints(final int hours) {
        final String uri = "http://localhost:8081/calculateStoryPoints?hours=" + hours;
        RestTemplate restTemplate = new RestTemplate();
        return Objects.requireNonNull(restTemplate.getForObject(uri, Double.class));
    }
}
